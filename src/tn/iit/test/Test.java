package tn.iit.test;

import tn.iit.observer.CurrentConditionsDisplay;
import tn.iit.observer.ForecastDisplay;
import tn.iit.observer.StatisticsDisplay;
import tn.iit.subject.WeatherData;

import java.awt.font.FontRenderContext;

public class Test {
    public static void main(String[] args) {
        WeatherData wd = new WeatherData();
        CurrentConditionsDisplay ccd = new CurrentConditionsDisplay();
        StatisticsDisplay sd = new StatisticsDisplay();
        wd.registerObserver(ccd);
        wd.registerObserver(sd);
        wd.measurementChanged(10 ,1040,40);
        System.out.println("----------------------------------------------------------------");

         ForecastDisplay fd = new ForecastDisplay();
         wd.registerObserver(fd);
         wd.removeObserver(ccd);
         wd.measurementChanged(8 ,1030,50);

    }
}

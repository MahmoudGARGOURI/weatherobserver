package tn.iit.observer;

public class CurrentConditionsDisplay implements Observer {
    @Override
    public void update(int temperature, int pressure, int humidity) {
        display();
        System.out.print("temperature: " + temperature + "\t");
        System.out.print("pressure: " + pressure + "\t");
        System.out.print("humidity: " + humidity + "\n");
    }

    @Override
    public void display() {
        System.out.println("CurrentConditionsDisplay" );
    }
}
